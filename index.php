<!DOCTYPE html>
<html>
	<head>
        <style>
			.container {
            	height: 370px;
            	padding-bottom: 10px;
            }

            #local, #remote {
                border-style: solid;
                float: left;
                margin-right: 5px;
            }
            
            #local {
                width: 320px;
                height: 240px;
                margin-top: 120px;
            }
            
            #remote {
                width: 480px;
                height: 360px;

            }

            .network-status {
            	height: 100%;
            }

            .network-quality {
            	width: 25px;
            	text-align: center;
            	height: 0%;
            	background-color: green;
            	float: left;
            	margin: 5px;
            	color: white;
            }
            
        </style>
    </head>
    <body>
    	<div class="container">
	        <div id='remote'></div>
	        <div id='local'></div>

	        <!-- Logging area for network messages -->
	        <textarea cols=40 class='network-status'></textarea>
	        <div class='network-quality'></div>
        </div>
        <form id='dial'>
            <input type='text' name='number'></input>
            <button type='submit'>Dial</button>
            <button id='hangup'>Hangup</button>
        </form>
    </body>

	<!-- Import the FCSDK JavaScript libraries -->
	<script type='text/javascript' src='http://fusion.vbox:8080/gateway/adapter.js'></script>
	<script type='text/javascript' src='http://fusion.vbox:8080/gateway/fusion-client-sdk.js'></script>

	<!-- Although not required by the FCSDK, jQuery will ease interactions with the DOM -->
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>

	<script>
		<?php 
			// import our init script
			include_once('init.php');

			// determine the parameters to use for the session
			$username = array_key_exists('username', $_GET) 
				? $_GET['username'] 
				: 'nathan';
			$password = array_key_exists('password', $_GET)
				? $_GET['password'] 
				: 'abc';

			// provision the session
			$token = provision($username, $password);
		?>
		
		var token = <?= $token ?>;

		// initialise the connection to the Gateway
		UC.start(token.sessionid);

		// when available, direct the local media stream to the right DOM element
	    var local = $('#local')[0];
	    var remote = $('#remote')[0];
	    UC.phone.setPreviewElement(local);

	    // wire up the call handler
	    $('#dial').submit(function (event) {
		    event.preventDefault();
		    var number = $(this).find('[name=number]').val();

		    // create the call
		    var call = UC.phone.createCall(number);
		    
		    // set the remote video element
		    call.setVideoElement(remote);

		    // add the relevant listeners to the call
		    addListenersTo(call);
		    
		    // now dial the call
		    call.dial();
		});

	    // wire up the hangup handler
		$('#hangup').click(function(event) {
			// prevent the button from causing the form to submit
			event.preventDefault();

		    // if we're on a call - end it!
		    var calls = UC.phone.getCalls();
		    var call = calls[0];
		    if (call) {
		        call.end();
		    }
		});

		// setup an incoming call handler
		UC.phone.onIncomingCall = function(call) {
			// prompt the user to accept or reject the incoming call
		    if (window.confirm('Answer call from: ' + call.getRemoteAddress() + '?')) {
		    	// set the target for remote video & answer the call
		    	call.setVideoElement(remote);

		    	// add the listeners
		    	addListenersTo(call);

		    	// answer the call
		        call.answer();
		    }
		    else {
		    	// reject the call
		        call.end();
		    }
		};

		// DOM element to be used to indicate network events
		var $networkStatus = $('.network-status');
		function addToLog (message) {
			var text =  new Date().toLocaleTimeString() + ': ' + message + '\n' + $networkStatus.val();
			$networkStatus.val(text);
		}

		//////////////////////////////////
		//
		// Register network callbacks

		UC.onConnectionRetry = function (attempt, delayUntilNextRetry) {
			addToLog('onConnectionRetry attempt: [' + attempt + '], next try: [' + delayUntilNextRetry + ']');
		};

		UC.onConnectivityLost = function () {
			addToLog('onConnectivityLost');
		};

		UC.onConnectionReestablished = function () {
			addToLog('onConnectionReestablished');
		};

		UC.onNetworkUnavailable = function () {
			addToLog('onNetworkUnavailable');
		};


		/////////////////////////////
		//
		// Call quality indicator
		var $indicator = $('.network-quality');

		// assigns listeners to the call
		function addListenersTo(call) {
			call.onConnectionQualityChanged = function (value) {
				$indicator.text(value)
				          .height(value + '%');
			};
		}

	</script>
</html>